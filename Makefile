CC=gcc
CFLAGS=-O3 -Wall

OBJS= fe.o key_exchange.o seed.o sc.o keypair.o sha512.o ge.o verify.o add_scalar.o sign.o

test: test.o $(OBJS)
	gcc -o $@ $^

clean:
	rm -f *.o test

